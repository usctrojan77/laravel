<!DOCTYPE html>
<html lang="en">
<head>
    <title>Laravel Application</title>

    <!-- CSS And JavaScript -->
</head>

<body>
<div class="container">
    <nav class="navbar navbar-default">
        <!-- Navbar Contents -->
        <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/albums">Albums</a></li>
        </ul>

    </nav>
</div>
@yield('content')
<script type="text/javascript">
    function filterAlbums(id) {
        window.location.href = "{{ URL::action('AlbumController@index') }}" +'?bandID=' + id;
    }
</script>
</body>
</html>
