@extends('layouts.app')

@section('content')
    <div class="panel-body">
        @include('errors.validationerrors')
        @if($opern == 'create')
            <form action="/bandeditcreate/1/0" method="POST" class="form-horizontal">
                @else
                    <form action="/bandeditcreate/0/{{$band->id}}" method="POST" class="form-horizontal">
                        @endif
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="band-name" class="col-sm-3 control-label">Band Name</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="band-name" class="form-control" value="{{ old('name',  isset($band->name) ? $band->name : null) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="band-start_date" class="col-sm-3 control-label">Start Date</label>

                            <div class="col-sm-6">
                                <input type="text" name="start_date" id="band-start_date" class="form-control" value="{{ old('start_date',  isset($band->start_date) ? $band->start_date : null) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="band-website" class="col-sm-3 control-label">Website</label>

                            <div class="col-sm-6">
                                <input type="text" name="website" id="band-website" class="form-control" value="{{ old('website',  isset($band->website) ? $band->website : null) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="band-still_active" class="col-sm-3 control-label">Still Active</label>

                            <div class="col-sm-6">
                                <input type="text" name="still_active" id="band-still_active" class="form-control" value="{{ old('still_active',  isset($band->still_active) ? $band->still_active : null) }}">
                            </div>
                        </div>
                        @if(!empty($albumItems))
                            <div class="form-group">
                                <label for="band-name" class="col-sm-3 control-label">Albums</label>
                                <div class="col-sm-6">
                                    <ul>
                                    @foreach ($albumItems as $album)
                                        <li>{{$album->name}}</li>
                                    @endforeach
                                    </ul>
                            </div>
                                @endif
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-plus"></i> @if($opern == 'create')
                                        Create Band
                                    @else
                                        Edit Band
                                    @endif
                                </button>
                            </div>
                        </div>

                    </form>
    </div>

@endsection