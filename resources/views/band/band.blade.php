@extends('layouts.app')

@section('content')

    <div class="panel-body">
        @include('errors.validationerrors')
        <p><a href="/bandcreate">Create Band</a></p>
        <table class="table">
            <tr>
                @foreach(array_keys($columns) as $column)
                    <th>
                    @if ($sortby == $columns[$column] && $order == 'asc')
                        {{ link_to_route(
                          $action,
                          $column, ['sortby' => $columns[$column],'order' => 'desc']
                        ) }}
                    @else
                        {{ link_to_route(
                          $action,
                          $column, ['sortby' => $columns[$column],'order' => 'asc']
                        ) }}
                    @endif
                    </th>
                @endforeach
                <th>Action</th>
            </tr>
            @foreach($items as $member)
                <tr>
                    <td>{{$member->id}}</td>
                    <td>{{$member->name}}</td>
                    <td>{{$member->start_date}}</td>
                    <td>{{$member->website}}</td>
                    <td>{{$member->still_active}}</td>
                    <td><a href="/bandedit/{{$member->id}}">Edit</a> | <a href="/banddelete/{{$member->id}}">Delete</a></td>
                </tr>
            @endforeach
        </table>
    </div>

@endsection