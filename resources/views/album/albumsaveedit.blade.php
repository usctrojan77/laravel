@extends('layouts.app')

@section('content')
    <div class="panel-body">
        @include('errors.validationerrors')
        @if($opern == 'create')
            <form action="/albumseditcreate/1/0" method="POST" class="form-horizontal">
        @else
                    <form action="/albumseditcreate/0/{{$album->id}}" method="POST" class="form-horizontal">
        @endif
            {{ csrf_field() }}
                        <div class="form-group">
                            <label for="album-name" class="col-sm-3 control-label">Album Name</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="album-name" class="form-control" value="{{ old('name',  isset($album->name) ? $album->name : null) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bandID">Select Band:</label>
                            <select class="form-control" id="bandID" name="bandID">
                                <option value="">Select a Band</option>
                                @foreach ($bandItems as $category)
                                    @if(isset($album->id))
                                        @if($category->id == $album->band_id)
                                            <option selected="selected" value="{{ $category->id }}">{{ $category->name }}</option>
                                        @else
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="album-recorded_date" class="col-sm-3 control-label">Recorded Date</label>

                            <div class="col-sm-6">
                                <input type="text" name="recorded_date" id="album-recorded_date" class="form-control" value="{{ old('recorded_date',  isset($album->recorded_date) ? $album->recorded_date : null) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="album-release_date" class="col-sm-3 control-label">Release Date</label>

                            <div class="col-sm-6">
                                <input type="text" name="release_date" id="album-release_date" class="form-control" value="{{ old('release_date',  isset($album->release_date) ? $album->release_date : null) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="album-number_of_tracks" class="col-sm-3 control-label">Number of Tracks</label>

                            <div class="col-sm-6">
                                <input type="text" name="number_of_tracks" id="album-number_of_tracks" class="form-control" value="{{ old('number_of_tracks',  isset($album->number_of_tracks) ? $album->number_of_tracks : null) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="album-label" class="col-sm-3 control-label">Label</label>

                            <div class="col-sm-6">
                                <input type="text" name="label" id="album-label" class="form-control" value="{{ old('label',  isset($album->label) ? $album->label : null) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="album-producer" class="col-sm-3 control-label">Producer</label>

                            <div class="col-sm-6">
                                <input type="text" name="producer" id="album-producer" class="form-control" value="{{ old('producer',  isset($album->producer) ? $album->producer : null) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="album-genre" class="col-sm-3 control-label">Genre</label>

                            <div class="col-sm-6">
                                <input type="text" name="genre" id="album-genre" class="form-control" value="{{ old('genre',  isset($album->genre) ? $album->genre : null) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-plus"></i> @if($opern == 'create')
                                                                   Create Album
                                                                @else
                                                                   Edit Album
                                                                @endif
                                </button>
                            </div>
                        </div>

        </form>
    </div>

@endsection