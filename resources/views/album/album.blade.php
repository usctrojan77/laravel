@extends('layouts.app')

@section('content')

    <div class="panel-body">
        @include('errors.validationerrors')
        Select Band <select onchange="filterAlbums(this.value);" class="form-control" name="category">
            @foreach ($bandItems as $category)
                @if($category->id == app('request')->input('category'))
                    <option selected="selected" value="{{ $category->id }}">{{ $category->name }}</option>
                @else
                     <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endif
            @endforeach
        </select>

        <p><a href="/albumscreate">Create Album</a></p>

   <table class="table">
       <tr>
           @foreach(array_keys($columns) as $column)
               <th>
                   @if ($sortby == $columns[$column] && $order == 'asc')
                       {{ link_to_route(
                         $action,
                         $column, ['sortby' => $columns[$column],'order' => 'desc']
                       ) }}
                   @else
                       {{ link_to_route(
                         $action,
                         $column, ['sortby' => $columns[$column],'order' => 'asc']
                       ) }}
                   @endif
               </th>
           @endforeach
           <th>Action</th>
       </tr>
       @foreach($items as $member)
           <tr>
               <td>{{$member->id}}</td>
               <td>{{$member->band_id}}</td>
               <td>{{$member->name}}</td>
               <td>{{$member->recorded_date}}</td>
               <td>{{$member->release_date}}</td>
               <td>{{$member->number_of_tracks}}</td>
               <td>{{$member->label}}</td>
               <td>{{$member->producer}}</td>
               <td>{{$member->genre}}</td>
               <td><a href="/albumedit/{{$member->id}}">Edit</a> | <a href="/albumdelete/{{$member->id}}">Delete</a></td>
           </tr>
       @endforeach
   </table>
</div>

@endsection