<?php

namespace App\CTraits;

use Illuminate\Http\Request;

trait SortableTrait {
    protected  $sortby;
    protected  $order;
    
    public function getSortedItems(Request $request, $model, $bandID = null)
    {
        $this->sortby = $request->input('sortby');
        $this->order = $request->input('order');
        if ($bandID && is_numeric($bandID)) {
            return $model::where('band_id','=',$bandID)->orderBy('created_at', 'asc')->get();
        }
        if ($this->sortby && $this->order) {
            return $model::orderBy($this->sortby, $this->order)->get();
        } else {
            return $model::all();
        }
    }
}