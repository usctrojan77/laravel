<?php


namespace App\Repository;

use App\Album;
use Illuminate\Http\Request;

class AlbumRepository
{

    public function getAlbum($id) {
        return Album::where('id', $id)
            ->first();
    }

    public function createAlbum(Request $request) {
        $album = new Album();
        $album->name = $request->name;
        $album->band_id = $request->bandID;
        $album->recorded_date = $request->recorded_date;
        $album->release_date = $request->release_date;
        $album->number_of_tracks = $request->number_of_tracks;
        $album->label = $request->label;
        $album->producer = $request->producer;
        $album->genre = $request->genre;
        $album->save();
    }

    public function editAlbum(Request $request, $id) {
        $album = Album::find($id);

        $album->name = $request->name;
        $album->band_id = $request->bandID;
        $album->recorded_date = $request->recorded_date;
        $album->release_date = $request->release_date;
        $album->number_of_tracks = $request->number_of_tracks;
        $album->label = $request->label;
        $album->producer = $request->producer;
        $album->genre = $request->genre;

        $album->save();
    }

    public function deleteAlbum($id) {
        Album::destroy($id);
    }
    
    public function getAlbumsForBand($id) {
        return Album::where('band_id',$id)
            ->get();
    }
}