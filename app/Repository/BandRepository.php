<?php


namespace App\Repository;

use App\Band;
use Illuminate\Http\Request;

class BandRepository
{
    public function getAllBand() {
        return Band::all(['id','name']);
    }

    public function deleteBand($id) {
        Band::destroy($id);
    }

    public function getBand($id) {
        return Band::where('id', $id)
            ->first();
    }

    public function createBand(Request $request) {
        $band = new Band();
        $band->name = $request->name;
        $band->start_date = $request->start_date;
        $band->website = $request->website;
        $band->still_active = $request->still_active;
        $band->save();
    }

    public function editBand(Request $request, $id) {
        $band = Band::find($id);

        $band->name = $request->name;
        $band->start_date = $request->start_date;
        $band->website = $request->website;
        $band->still_active = $request->still_active;
        $band->save();
    }
}