<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table = 'album';

    public static $columns = [
        'ID' => 'id',
        'Band' => 'band_id',
        'Name' => 'name',
        'Recorded Date' => 'recorded_date',
        'Release Date' => 'release_date',
        'Number of Tracks' => 'number_of_tracks',
        'Label' => 'label',
        'Producer' => 'producer',
        'Genre' => 'genre'
    ];

    /**
     * Get the band which owns this album
     */
    public function band() {
        return $this->belongsTo('App\Band');
    }
}
