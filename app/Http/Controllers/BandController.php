<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Band;
use App\CTraits\SortableTrait;
use App\Repository\BandRepository;
use App\Repository\AlbumRepository;
use Illuminate\Routing\Redirector;

class BandController extends Controller
{
    use SortableTrait;
    protected $albumRepo;
    protected $bandRepo;

    public function __construct(AlbumRepository $albumRepository, BandRepository $bandRepository) {
        $this->albumRepo = $albumRepository;
        $this->bandRepo = $bandRepository;
    }

    public function index(Request $request)
    {
        $items = $this->getSortedItems($request,'App\Band');
        $sortby = $this->sortby;
        $order = $this->order;

        $action = 'home';
        $columns = Band::$columns;

        return view('band.band', compact('items', 'sortby', 'order', 'columns', 'action'));
    }
    
    public function deleteBand(Request $request, $id) {
        $this->bandRepo->deleteBand($id);
        return redirect()->route('home');
    }

    public function bcreate(Request $request) {
        $opern = 'create';
       
        return view('band.bandsaveedit',compact('opern'));
    }

    public function bedit(Request $request, $id) {
        $band = $this->bandRepo->getBand($id);
        $opern = '';
        $albumItems = $this->albumRepo->getAlbumsForBand($id);

        return view('band.bandsaveedit',compact('opern', 'band', 'albumItems'));
    }

    public function savecreate(Redirector $redirect, Request $request, $opern = null, $id = 0) {
        $this->validate($request ,['name' => 'required']);
        if (!empty($opern)) {
            $this->bandRepo->createBand($request);
        }
        else {
            $this->bandRepo->editBand($request, $id);
        }

        return redirect()->route('home');
    }
}