<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;
use App\CTraits\SortableTrait;
use App\Repository\AlbumRepository;
use App\Repository\BandRepository;
use Illuminate\Routing\Redirector;

class AlbumController extends Controller
{
    use SortableTrait;

    protected $albumRepo;
    protected $bandRepo;

    public function __construct(AlbumRepository $albumRepository, BandRepository $bandRepository) {
        $this->albumRepo = $albumRepository;
        $this->bandRepo = $bandRepository;
    }

    public function index(Request $request)
    {
        $bandID = $request->input('bandID');
        $items = $this->getSortedItems($request,'App\Album', $bandID);
        $sortby = $this->sortby;
        $order = $this->order;

        $action = 'albumhome';
        $columns = Album::$columns;
        $bandItems = $this->bandRepo->getAllBand();

        return view('album.album', compact('items', 'sortby', 'order', 'columns', 'action','bandItems'));
    }
    
    public function acreate(Request $request) {
        $bandItems = $this->bandRepo->getAllBand();
        $opern = 'create';
        return view('album.albumsaveedit',compact('bandItems', 'opern'));
    }
    
    public function aedit(Request $request, $id) {
        $album = $this->albumRepo->getAlbum($id);
        $opern = '';
        $bandItems = $this->bandRepo->getAllBand();

        return view('album.albumsaveedit',compact('bandItems', 'opern', 'album'));
    }
    
    public function savecreate(Redirector $redirect, Request $request, $opern = null, $id = 0) {
        $this->validate($request ,['name' => 'required', 'bandID' => 'required']);
        if (!empty($opern)) {
            $this->albumRepo->createAlbum($request);
        }
        else {
            $this->albumRepo->editAlbum($request, $id);
        }

        return redirect()->route('albumhome');
    }

    public function deleteAlbum(Request $request, $id) {
        $this->albumRepo->deleteAlbum($id);
        return redirect()->route('albumhome');
    }
}