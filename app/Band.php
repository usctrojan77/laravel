<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Band extends Model
{
    protected $table = 'band';

    public static $columns = [
        'ID' => 'id',
        'Name' => 'name',
        'Start Date' => 'start_date',
        'Website' => 'website',
        'Still Active' => 'still_active'
    ];

    /**
     * Get the albums for the band
     */
    public function album() {
        return $this->hasMany('App\Album');
    }
}
