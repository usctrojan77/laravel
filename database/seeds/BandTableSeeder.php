<?php

use Illuminate\Database\Seeder;

class BandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 25;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('band')->insert([
                'name' => $faker->name,
                'start_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'website' => $faker->domainName,
                'still_active' => 'Y',
                'created_at' => $faker->dateTime
            ]);
        }
    }
}
