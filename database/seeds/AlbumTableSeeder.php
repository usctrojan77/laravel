<?php

use Illuminate\Database\Seeder;
use App\Band;

class AlbumTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 100;

        $band = Band::all()->pluck('id');

        for ($i=0; $i<$limit; $i++) {
            DB::table('album')->insert([
                'band_id' => $faker->randomElement($band->toArray()),
                'name' => $faker->name,
                'recorded_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'release_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'number_of_tracks' => $faker->randomNumber(2),
                'label' => $faker->text,
                'producer' => $faker->name,
                'genre' => $faker->word,
                'created_at' => $faker->dateTime
            ]);
        }
    }
}
