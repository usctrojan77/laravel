<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('album', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('band_id');
            $table->string('name', 255);
            $table->date('recorded_date');
            $table->date('release_date');
            $table->integer('number_of_tracks');
            $table->string('label', 255);
            $table->string('producer', 100);
            $table->string('genre', 100);
            $table->timestamps();
            $table->foreign('band_id')->references('id')->on('band')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('album');
    }
}
