<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['uses' => 'BandController@index', 'as' => 'home']);

Route::get('/albums', ['uses' => 'AlbumController@index', 'as' => 'albumhome']);

Route::get('/albumscreate', ['uses' => 'AlbumController@acreate', 'as' => 'alcreate']);

Route::get('/albumedit/{id}', ['uses' => 'AlbumController@aedit', 'as' => 'aledit']);

Route::post('/albumseditcreate/{opern}/{id}', ['uses' => 'AlbumController@savecreate', 'as' => 'savecreatealbum']);

Route::get('/albumdelete/{id}', ['uses' => 'AlbumController@deleteAlbum', 'as' => 'delalbum']);

Route::get('/banddelete/{id}', ['uses' => 'BandController@deleteBand', 'as' => 'delband']);

Route::get('/bandcreate', ['uses' => 'BandController@bcreate', 'as' => 'bancreate']);

Route::get('/bandedit/{id}', ['uses' => 'BandController@bedit', 'as' => 'banedit']);

Route::post('/bandeditcreate/{opern}/{id}', ['uses' => 'BandController@savecreate', 'as' => 'savecreateband']);
